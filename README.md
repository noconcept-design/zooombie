# Zooombie

Ist ein "Tower Defense" Spiel geschreiben in JavaScript.   
(noch nicht fertig)

Das YT-Tutorial dazu ist von [Franks laboratory](https://www.youtube.com/watch?v=QxYg8-mhhhs) - meiner Meinung nach ein viel zu 
unbekannter Kanal.    
Wobei ich klein aber fein eh besser finde.

Frank erklärt hier Einsteiger-freundlich auch die komplexeren Themen. 

Einfachmal vorbeischauen, und eventuell wie ich, mitmachen ;-) 

![screenshot](assets/images/screenshot.png)
![screenshot](assets/images/screenshot2.png)
![screenshot](assets/images/screenshot3.png)